const Discord = require("discord.js");
const config = require("../config.js");

module.exports.run = async (client, guildid, prefix, message, args) => {

  const user = message.mentions.users.first() || message.author;
  const member = message.mentions.members.first() || message.member;

  let game = user.presence.game;
  let createdAt = user.createdAt;
  let joinedAt = member.joinedAt;

  let embed = new Discord.RichEmbed()
    .setColor(`#c6e000`)
    .setAuthor(user.tag, user.displayAvatarURL)
    .setThumbnail(user.displayAvatarURL)
    .setURL(user.displayAvatarURL)
    .addField("Server Nickname", member.nickname == null ? 'N/A' : member.nickname, true)
    .addField("Status", user.presence.status.toUpperCase(), true)
    .addField("Account Created", `${getDayName(createdAt.getDay())}, ${createdAt.getDate()}${getDateN(createdAt.getDate())} ${getMonthName(createdAt.getMonth())} ${createdAt.getFullYear()}  |  ${createdAt.getUTCHours()}:${minuteLong(createdAt.getMinutes())}:${createdAt.getSeconds()}`, false)
    .addField("Join Date", `${getDayName(joinedAt.getDay())}, ${joinedAt.getDate()}${getDateN(joinedAt.getDate())} ${getMonthName(joinedAt.getMonth())} ${joinedAt.getFullYear()}  |  ${joinedAt.getUTCHours()}:${minuteLong(joinedAt.getMinutes())}:${joinedAt.getSeconds()}`, false)
    // .addField("Join Position", member.guild.joinedTimestamp, false) // IDK how to get the Join Position
    .addField("Highest Role", member.highestRole, true)
    .addField("Category Role", member.hoistRole ? member.hoistRole : `Online`, true)
  await sortRoles(member.roles.array(), embed);
  await setGame(game, embed);

  // console.log(member.roles.array());

  return message.channel.send(embed);
}

module.exports.help = {
  name: "userinfo",
  type: "Command"
}

// Sets the game to "nothing", "Spotify" or the respective game, so that no error can happen.
async function setGame(game, embed) {
  if (game == null) {
    return embed.setDescription(`is doing nothing.`);
  } else if (game.name === `Spotify`) {
    return embed.setDescription(`is listening on **Spotify**`);
  } else {
    return embed.setDescription(`is playing **${game.name}**`);
  }
}

async function sortRoles(roles, embed) {
  roles.sort(function (a, b) {
    if (a.position < b.position) {
      return -1;
    }
    if (a.position > b.position) {
      return 1;
    }
    return 0;
  })

  embed.addField(`Roles [${roles.length - 1}]`, roles.splice(1).reverse(), false);
}

// Translates day number to the day name
function getDayName(day) {
  if (day == '0') return `Sunday`;
  if (day == '1') return `Monday`;
  if (day == '2') return `Tuesday`;
  if (day == '3') return `Wednesday`;
  if (day == '4') return `Thursday`;
  if (day == '5') return `Friday`;
  if (day == '7') return `Monday`;
  return `Unknown`; //Should never happen
}

// Appends this to day
function getDateN(date) {
  if (date.toString().endsWith(`1`)) return `st`;
  if (date.toString().endsWith(`2`)) return `nd`;
  if (date.toString().endsWith(`3`)) return `rd`;
  return `th`;
}

// If minute only has one number, adds a 0 at the beginning.
function minuteLong(minute) {
  if (minute.toString().length === 1) return `0${minute}`;
  return minute;
}

// Translates month number to the month name
function getMonthName(month) {
  if (month == `0`) return `January`;
  if (month == `1`) return `February`;
  if (month == `2`) return `March`;
  if (month == `3`) return `April`;
  if (month == `4`) return `May`;
  if (month == `5`) return `June`;
  if (month == `6`) return `July`;
  if (month == `7`) return `August`;
  if (month == `8`) return `September`;
  if (month == `9`) return `October`;
  if (month == `10`) return `November`;
  if (month == `11`) return `December`;
  return `Unknown` // Should never happen
}