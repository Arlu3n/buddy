const Discord = require("discord.js");
const config = require("../config.js");

module.exports.run = async (client, guildid, prefix, message, args) => {

  const channel = message.channel;

  channel.send("Please wait, this may take some time...");
  botfetchmessages(channel);

  return;
}

module.exports.help = {
  name: "fetch",
  type: "Command"
}

async function botfetchmessages(channel) {

  let messagecount = 0;
  let tempmessageid = 0;

  channel.fetchMessages({ limit: 100 })
    .then(messages => {

      messagecount = messages.size;
      tempmessageid = messages.last().id;

      if (messages.size === 100) {

        var interval = setInterval(function () {

          new Promise(function (resolve) {

            channel.fetchMessages({ limit: 100, before: tempmessageid })
              .then(whilemessages => {

                messagecount = messagecount + whilemessages.size;
                tempmessageid = whilemessages.last().id;

                if (whilemessages.size !== 100) {
                  clearInterval(interval);
                  resolve();
                  return channel.send(`Received ${messagecount} messages in this channel`);
                }

              })

          });

        }, 1000)

      } else { return channel.send(`Received ${messagecount} messages in this channel`); }

      return;

    }).catch(console.error);
}
