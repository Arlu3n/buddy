const Discord = require("discord.js");
const fs = require("fs");

const config = require("./config.js");

const misc = require("./misc.js");
var colors = misc.colors;
var prefixes = misc.prefixes;

const client = new Discord.Client({ disableEveryone: true });
client.commands = new Discord.Collection();
loadCommands();

client.on("ready", async () => {
	console.log(`${prefixes.ready} Logged in as ${client.user.tag} (${client.user.id}) on ${client.guilds.array().length} server(s).${colors.Reset}`);
	console.log(`${prefixes.ready} Ready at: ${client.readyAt}${colors.Reset}`);
});

client.on("message", async message => {
	if (message.author.bot === true) return;
	if (message.channel.type === "dm") return;
	if (message.channel.type === "group") return;

	let cmd = message.content.split(" ")[0];

	let guildid = message.guild.id;
	let prefix = config.cmd_prefix;
	let args = message.content.split(" ").slice(1);

	let exec = client.commands.get(cmd.slice(prefix.length));
	if (exec){
		exec.run(client, guildid, prefix, message, args)
		console.log(`${prefixes.log} ${exec.help.name}.js executed on ${message.guild.name} (${message.guild.id})!`);
	} 
})

client.login(config.bot.token);

async function loadCommands() {
	fs.readdir("./commands/", (err, files) => {
		if (err) console.log(`${error}` + err);

		let jsfiles = files.filter(f => f.split(`.`).pop() === `js`)
		if (jsfiles.length <= 0) {
			console.log(`${error} Could not find commands!`)
			return;
		}

		jsfiles.forEach((file) => {
			let f = require(`./commands/${file}`);
			client.commands.set(f.help.name, f);
			if(config.debug === true) {console.log(`${prefixes.debug} (${f.help.type}) ${file} loaded!`);}
		});
		console.log(`${prefixes.info} Loading Commands... ${colors.FgGreen}DONE!${colors.Reset}`)
	});
}